// odoo.define('post.list', function (require) {
//     "use strict";

//     // let ajax = require('web.ajax');

//     $(document).ready(function () {
//         $('form#order_submit #post_city input').on('input', function () {
//             getDepartments()
//         });

//         $('form#order_submit #post_city input').click(function () {
//             getDepartments();
//         });

//         $('form#order_submit #post_city input').attr('autocomplete', 'disable');

//         $('.delivery .inputs label[for="post"] input').click(function () {
//             getDepartments();
//         });

//         $('.delivery div.inputs p label input').change(function () {
//             $('div.delivery .inputs #post_number select option').remove();
//             $('div.delivery .inputs #post_city datalist option').remove();
//             if ($('.delivery .inputs label[for="post"] input').is(':checked')) {
//                 $.post('/get-new-post-city')
//                     .done(function (data) {
//                         $('.delivery div.inputs #city').replaceWith(data);
//                     });
//             }

//         });

//         function getDepartments(ui) {
//             if ($('.delivery .inputs label[for="post"] input').is(':checked')) {
//                 $.post('/get-new-post-department', {
//                     'city': ui ? ui.item.value : $('form#order_submit #post_city input').val()
//                 }, function (data) {
//                     $('form#order_submit #post_number select').replaceWith(data)
//                 });
//             }
//         }

//     });

// });