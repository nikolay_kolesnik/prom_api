# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Prom API',
    'author': "Nikolay Kolesnyk",
    'website': "http://prom.ua",
    'version': '0.1',
    'summary': 'Send',
    'sequence': 30,
    'description': """ Интеграция с маркетплейсом PROM UA
    """,
    'category': 'Extra Tools',
    'depends': ['website', 'sale', 'sale_management', 'purchase', 'website_sale'],
    'data': [
        'security/ir.model.access.csv',
        'views/prom_api.xml',
    ],
    'application': True,
}
