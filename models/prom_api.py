# -*- coding: utf-8 -*-

import json
import http.client
import pprint
import requests
import logging

from odoo import api, fields, models, http, _
from odoo.http import request, Response

_logger = logging.getLogger(__name__)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class PromAPI(models.Model):
    _name = 'prom.api'

    name = fields.Char(string='Name', default='Prom API Token', required=True)
    host = fields.Char(string='Host', required=True, default='my.prom.ua')
    token = fields.Char(string='Token', required=True)
    active = fields.Boolean(string='Active', default=True)

    #####################
    ###### P R O M ######
    #####################

    @api.multi
    def make_request(self, method, url, body=None):

        api_connect =  self.search([('active', '=', True)])

        if not api_connect.token:
            raise Exception('Sorry, there\'s no any AUTH_TOKEN!')

        method = {'GET': requests.get, 'POST': requests.post}[method]
        headers = {'Authorization': 'Bearer {}'.format(api_connect.token),
                   'Content-type': 'application/json'}
        response = method('http://'+api_connect.host+url, headers=headers, json=body)
        return response.json()

    @api.multi
    def get_order_list(self):
        url = '/api/v1/orders/list'
        method = 'GET'

        return self.make_request(method, url)

    @api.multi
    def get_order(self, order_id):
        url = '/api/v1/orders/{id}'
        method = 'GET'

        return self.make_request(method, url.format(id=order_id))

    @api.multi
    def set_order_status(self, status, ids, cancellation_reason=None, cancellation_text=None):
        url = '/api/v1/orders/set_status'
        method = 'POST'

        body = {
            'status': status,
            'ids': ids
        }
        if cancellation_reason:
            body['cancellation_reason'] = cancellation_reason

        if cancellation_text:
            body['cancellation_text'] = cancellation_text

        return self.make_request(method, url, body)


    #####################
    ###### M A I N ######
    #####################

    @api.multi
    def process_orders(self):

        _logger.info('Get Prom.UA Orders')

        order_list = self.get_order_list()
        if not order_list['orders']:
            raise Exception('Sorry, there\'s no any order!')

        for order in order_list['orders']:
            if order['status'] == 'pending':
                print(bcolors.HEADER, order, bcolors.ENDC)
                self.create_sale(order)
                self.receive_order_request(order['id']) # Другая альтернативная функция для подтверждения заказа

        print( bcolors.HEADER , order_list['orders'][0], bcolors.ENDC)

        last_order = order_list['orders'][0]
        self.receive_order_request(last_order['id'])
        self.create_sale(last_order)


    #####################
    ###### O D O O ######
    #####################

    @api.multi
    def create_comment(self, order):
        client = self.to_str(order['client_first_name']) + self.to_str(order['client_last_name'])
        delivery = self.to_str(order['delivery_option']) + self.to_str(order['delivery_address'])
        return(str(
            client + '\n' +
            'Доставка: ' + delivery + '\n' +
            'Телефон: ' + self.to_str(order['phone'])
            ))

    @api.multi
    def create_sale(self, order):
        # Покупателя провалить в 'sale.order'
        # line_env = self.env['sale.order.line'].sudo()
        sale_order = self.env['sale.order'].sudo().create({
                        'state': 'draft',
                        'partner_id': 115, 
                        'pricelist_id': 1,
                        'invoice_status': 'to invoice',
                        'payment_type': 'cash',
                        'is_liqpay': False,
                        'note': self.create_comment(order),
                        'confirmation_date': order['date_created']
                        })

        # Продукт провалить в 'sale.order.line'
        for product in order['products']:
            product_id = self.env['product.product'].search([('id', '=', product['external_id'])])
            if product_id:
                new_line = self.env['sale.order.line'].sudo().create({
                        'product_id': product_id.id or product['external_id'],
                        'name':product_id.name or product['name'],
                        'order_id': sale_order.id,
                        'state': 'sale',
                        'qty_to_invoice' : product['quantity'],
                        'product_uom_qty' : product['quantity']
                        })                
                new_line.product_id_change() # Calling an onchange method to update the record
        # self.env['sale.order'].sudo().browse(sale_order.id).action_confirm()
        # sale_order.sudo().write({
        #     'state': 'sale'
        # })
        sale_order.marketplace_action_confirm()

    @api.multi
    def to_str(self, _string):
        if not _string or _string == 'None': return ''
        else: return str(_string)

    @api.multi
    def receive_order_request(self, order_id):
        token = self.search([('active', '=', True)]).token
        body = {
            "status": "received",
            "ids": [order_id]
        }
        headers = {'Authorization': 'Bearer {}'.format(token),
                'Content-type': 'application/json'}
        response = requests.post('https://my.prom.ua/api/v1/orders/set_status',
                                headers=headers, json=body)
        print(response.text)


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def marketplace_action_confirm(self):
        self.action_confirm()